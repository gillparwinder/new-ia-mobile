import Config from "react-native-config";
import DeviceInfo from "react-native-device-info";
export async function login(data, header) {
  const url = Config.CORE_ENDPOINT_BASE_URL + "/api/account/login";
  console.log(url + "login");
  let body = {
    user_name: data.user_name,
    password: data.password,
    device_id: 111
    //device_id: DeviceInfo.getUniqueID()
  };
  try {
    let response = await fetch(url, {
      method: "POST",
      headers: header,
      body: JSON.stringify(body)
    });

    return {
      status: response.ok,
      data: await response.json()
    };
  } catch (e) {
    console.error(e);
    throw e;
  }
}

export async function getAccessToken(data, header) {
  const url = Config.CORE_ENDPOINT_BASE_URL + "/api/account/token";
  try {
    let response = await fetch(url, {
      method: "POST",
      headers: header,
      body: JSON.stringify(data)
    });

    return {
      status: response.ok,
      data: await response.json()
    };
  } catch (e) {
    console.error(e);
    throw e;
  }
}

export async function forgotPassword(data, header) {
  const url =
    Config.CORE_ENDPOINT_BASE_URL + "/api/account/requestpasswordreset";
  console.log(url + "  forgotpassword");
  let body = { user_name: data.user_name };
  try {
    let response = await fetch(url, {
      method: "POST",
      headers: header,
      body: JSON.stringify(body)
    });

    return {
      status: response.ok,
      data: await response.json()
    };
  } catch (e) {
    console.error(e);
    throw e;
  }
}

export async function getMememerEneties(data, header) {
  const url = Config.CORE_ENDPOINT_BASE_URL + "/api/account/memberentities";
  console.log(url + "  guestverify");
  try {
    let response = await fetch(url, {
      method: "GET",
      headers: header
    });

    return {
      status: response.ok,
      data: await response.json()
    };
  } catch (e) {
    console.error(e);
    throw e;
  }
}

export async function validateGuestUser(data, header) {
  const url =
    Config.CORE_ENDPOINT_BASE_URL + "/api/account/verify/entity/" + data.entity;
  console.log(url + "  guestverify");
  try {
    let response = await fetch(url, {
      method: "POST",
      headers: header
    });

    return {
      status: response.ok,
      data: await response.json()
    };
  } catch (e) {
    console.error(e);
    throw e;
  }
}

export async function resetPassword(data, header) {
  const url =
    Config.CORE_ENDPOINT_BASE_URL + "/api/account/resetpassword?t=" + data.access_token
  console.log(url + "  resetPassword");
  try {
    let response = await fetch(url, {
      method: "POST",
      headers: header,
      body: JSON.stringify({
        password: data.password,
        confirm_password: data.confirm_password
      })
    });

    return {
      status: response.ok,
      data: await response.json()
    };
  } catch (e) {
    console.error(e);
    throw e;
  }
}
