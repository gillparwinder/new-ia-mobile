import * as types from "../../../types/types";
import { NavigationActions } from "react-navigation";
const initialState = {
  processingRequest: false,
  refresh_token: "",
  auth_token: "",
  user_name: "",
  login_error: false,
  loggedIn: false,
  invalid_role: false,
  error_message: "",
  member_entity: "",
  entity_name: "",
  language: "",
  role: "",
  member_entities: [],
  passwordResetRequestSuccess: false,
  passwordResetRequestFailed: false,
  forgotPasswordRequestSuccess: false,
  forgotPasswordRequestFailed: false,
  resetPasswordResponse: {},
  feature_config: {
    allow_call: true,
    allow_record: false
  }
};

const loginReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.LOGIN_REQUEST:
      return {
        ...state,
        processingRequest: true,
        refresh_token: "",
        loggedIn: false,
        login_error: false,
        invalid_role: false,
        error_message: "",
        language: ""
      };

    case types.LOGIN_SUCCESS:
      return {
        ...state,
        processingRequest: false,
        login_error: false,
        loggedIn: true,
        refresh_token: action.payload.refresh_token,
        feature_config: action.payload.feature_config,
        error_message: ""
      };

    case types.LOGIN_FAILURE:
      return {
        ...state,
        login_error: true,
        processingRequest: false,
        refresh_token: false,
        loggedIn: false,
        error_message: action.payload.error_message
      };

    case types.LOGOUT_REQUEST:
      NavigationActions.navigate({ routeName: "Login" });
      return initialState;

    case types.ACCESS_TOKEN_REQUEST:
      return {
        ...state,
        access_token: ""
      };

    case types.RESET_ACCESS_TOKEN:
      return {
        ...state,
        access_token: ""
      };

    case types.ACCESS_TOKEN_SUCCESS:
      return {
        ...state,
        invalid_role: false,
        access_token: action.payload["access_token"],
        member_entity:
          state.member_entity == ""
            ? action.payload["member_entity"]
            : state.member_entity,
        entity_name: state.entity_name == "" ? action.payload["entity"] : state.entity_name,
        user_name: action.payload["user_name"],
        user_id: action.payload["user_id"],
        role: action.payload["role"]
      };

    case types.ACCESS_TOKEN_FAILURE:
      return {
        ...state,
        invalid_role: false,
        access_token: action.payload["access_token"]
        // member_entity: ""
      };

    case types.INVALID_ROLE:
      return {
        ...state,
        login_error: false,
        processingRequest: false,
        refresh_token: false,
        loggedIn: false,
        user_name: "",
        invalid_role: true
      };

    case types.RESET_ERROR:
      return {
        ...state,
        login_error: false
      };

    case types.FORGOT_PASSWORD_REQUEST:
      return {
        ...state,
        user_name: "",
        forgotPasswordRequestSuccess: false,
        forgotPasswordRequestFailed: false,
        processingRequest: true,
      };

    case types.FORGOT_PASSWORD_SUCCESS:
      return {
        ...state,
        user_name: action.payload.user_name,
        forgotPasswordRequestSuccess: true,
        forgotPasswordRequestFailed: false,
        processingRequest: false,
      };

    case types.FORGOT_PASSWORD_FAILURE:
      return {
        ...state,
        user_name: "",
        forgotPasswordRequestSuccess: false,
        forgotPasswordRequestFailed: true,
        processingRequest: false,
      };

    case types.RESET_PASSWORD_REQUEST:
      return {
        ...state,
        passwordResetRequestSuccess: false,
        passwordResetRequestFailed: false,
        processingRequest: true,
      };

    case types.RESET_PASSWORD_SUCCESS:
      return {
        ...state,
        passwordResetRequestSuccess: true,
        passwordResetRequestFailed: false,
        processingRequest: false,
        resetPasswordResponse: action.payload
      };

    case types.RESET_PASSWORD_FAILURE:
      return {
        ...state,
        passwordResetRequestSuccess: false,
        passwordResetRequestFailed: true,
        processingRequest: false,
        resetPasswordResponse: action.payload
      };

    case types.RESET_LOGIN:
      return {
        ...state,
        refresh_token: "",
        user_name: "",
        loggedIn: false,
        login_error: false,
        invalid_role: false,
        error_message: ""
      };

    case types.SET_LANGUAGE_REQUEST:
      return {
        ...state,
        language: action.payload.language
      };

    case types.SET_MEMBER_ENTITIES_REQUEST:
      return {
        ...state,
        member_entity: action.payload.entity,
        entity_name: action.payload.entity_name
      };

    case types.GET_MEMBER_ENTITIES_REQUEST:
      return {
        ...state,
        processingRequest: true,
        member_entities: []
      };

    case types.GET_MEMBER_ENTITIES_SUCCESS:
      return {
        ...state,
        processingRequest: false,
        member_entities: action.payload
      };

    case types.GET_MEMBER_ENTITIES_FAILURE:
      return {
        ...state,
        processingRequest: false,
        member_entities: []
      };

    case types.VALIDATE_GUEST_USER_REQUEST:
      return {
        ...state,
        processingRequest: true
      };

    case types.VALIDATE_GUEST_USER_SUCCESS:
      return {
        ...state,
        processingRequest: false,
        loggedIn: action.payload.status
      };

    case types.VALIDATE_GUEST_USER_FAILURE:
      return {
        ...state,
        processingRequest: false
      };

    default:
      return state;
  }
};

export default loginReducer;
