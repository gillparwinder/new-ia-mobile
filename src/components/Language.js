import React, { Component } from "react";
import {
    View,
    Text,
    TouchableOpacity,
    TextInput,
    Image,
    TouchableHighlight,
    StyleSheet,
    Dimensions,
    Modal
} from "react-native";
import { Picker } from '@react-native-community/picker';
import Toast from "react-native-tiny-toast";
const Item = Picker.Item;
const DEVICE_WIDTH = Dimensions.get("window").width;
const DEVICE_HEIGHT = Dimensions.get("window").height;
const languages = require("../assets/languages.json");
export default class Language extends Component {
    constructor(props) {
        super(props);
        this.state = {
            language: "",
        };
    }

    componentDidMount() {
        //this.setState({
        //    language: this.props.language
        //});
    }

    submit = () => {
        //this.props.setLanguage(this.state.language);
        //if (this.props.role == "GUEST_USER") {
        //  this.props.navigation.navigate("MemberEntity");
        //}
        //else if (this.props.role == "CENTRAL_SUPPORT_EXECUTIVE") {
        //  this.props.navigation.navigate("ListFieldExecutives");
        //}
        //else 
        if (this.state.language) {
            this.props.navigation.navigate("Home");
        } else {
            Toast.show("Please choose language")
        }
        //alert("home")
    };

    render() {
        return (
            <View style={styles.loginConatiner}>
                <View style={styles.logoConatiner}>
                    <View>
                        <Image
                            source={require("../assets/logo.png")}
                            resizeMode="contain"
                            style={{ width: 200, height: 200 }}
                        />
                    </View>
                    <View style={{ paddingTop: 30, alignItems: "center" }}>
                        <Text style={styles.loginText}>Default chat language</Text>
                    </View>
                </View>
                <View style={styles.lanContainer}>
                    <View style={styles.lanConatiner}>
                        <View style={{ paddingLeft: 10, paddingRight: 10, paddingTop: 5 }}>
                            <Image
                                source={require("../assets/language.png")}
                                resizeMode="contain"
                                style={{ width: 25, height: 25 }}
                            />
                        </View>
                        <View style={styles.selectOuter}>
                            {/* <View>
                <TouchableOpacity
                  style={{
                    height: 40,
                    width: "90%",
                    backgroundColor: "#3b4c69",
                    color: "#6780aa",
                    borderRadius: 5,
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "space-between"
                  }}
                  onPress={() => this.toggleModalVisible()}
                >
                  <Text style={{ color: "#ADADAD", paddingStart: 10 }}>
                    Select
                  </Text>
                  <View style={{ marginBottom: 12, right: 10 }}>
                    <Icon
                      name="sort-down"
                      type="font-awesome"
                      style={{
                        fontSize: 18,
                        color: "#ADADAD"
                      }}
                      color="#0a3c8e"
                    />
                  </View>
                </TouchableOpacity>
              </View> */}
                            {!this.state.PickerVisible ?
                                <TouchableOpacity onPress={() => {
                                    this.setState({ PickerVisible: true })
                                }}
                                    style={{
                                        height: 40,
                                        width: "90%",
                                        backgroundColor: "#3b4c69",
                                        color: "#6780aa",
                                        borderRadius: 5,
                                        justifyContent: "center"
                                    }}
                                >
                                    <Text style={{ paddingLeft: 10, color: "white" }}>{this.state.language ? this.state.language : "Choose language"}</Text>
                                </TouchableOpacity>
                                :
                                <Picker
                                    textStyle={{ color: "#6780aa", width: "100%" }}
                                    style={{ color: "#6780aa" }}
                                    mode="dialog"
                                    selectedValue={this.state.language}
                                    prompt="Select"
                                    onValueChange={value => this.setState({ language: value, PickerVisible: false })}
                                    itemStyle={{ backgroundColor: "#eee" }}
                                >
                                    <Picker.Item label="Choose language" />
                                    {languages.map((item, index) => (
                                        <Picker.Item
                                            key={index}
                                            label={item.name}
                                            value={item.name}
                                        />
                                    ))}
                                </Picker>}
                        </View>
                    </View>
                </View>
                <View style={styles.buttonouter}>
                    <TouchableOpacity
                        style={styles.loginButton}
                        onPress={() => this.submit()}
                    >
                        <Text style={styles.text}>Save</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    buttoncontainer: {
        width: "90%",
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "blue",
        height: 40
    },
    text: {
        textAlign: "center",
        height: 40,
        color: "white",
        paddingTop: 10
    },
    loginConatiner: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "#232d42",
        height: DEVICE_HEIGHT
    },
    resetPasswordSuccessContainer: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "#232d42",
    },
    backToginButton: {
        width: "90%",
        backgroundColor: "#3b4c69",
        marginLeft: 15,
        borderRadius: 5,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center'
    },
    logoConatiner: {
        flex: 2,
        justifyContent: "flex-end",
        display: "flex",
        flexDirection: "column"
    },
    loginText: {
        color: "white",
        fontWeight: "400",
        fontSize: 20
    },
    resetSuccssText: {
        color: "#fff",
        fontWeight: "100",
        fontSize: 16,
        textAlign: 'center'
    },
    textContainer: {
        flex: 2,
        justifyContent: "center",
        width: "100%",
        alignItems: "center"
    },
    emailConatiner: { display: "flex", flexDirection: "row" },
    emailOuter: {
        width: "60%"
    },
    emailInput: {
        height: 40,
        width: "90%",
        marginBottom: 20,
        backgroundColor: "#3b4c69",
        color: "#fff",
        borderRadius: 5,
        paddingLeft: 10
    },
    passwordConatier: { display: "flex", flexDirection: "row" },
    passwordOuter: { width: "60%" },
    passwordInput: {
        height: 40,
        width: "90%",
        // marginBottom: 20,
        backgroundColor: "#3b4c69",
        color: "#fff",
        borderRadius: 5,
        paddingLeft: 10
    },
    forgotContainer: {
        justifyContent: "flex-start",
        flexDirection: "column",
        width: "60%"
    },
    forgottext: {
        textAlign: "left",
        paddingLeft: 20,
        fontStyle: "italic",
        fontSize: 12,
        color: "white",
        paddingTop: 5
    },
    errorText: {
        color: "red",
        fontStyle: "italic",
        fontSize: 12,
        paddingLeft: 20
    },
    buttonouter: { width: "60%", flex: 1 },
    loginButton: {
        width: "90%",
        backgroundColor: "#3b4c69",
        marginLeft: 15,
        borderRadius: 5,
        height: 40
    },
    lanContainer: {
        flex: 2,
        justifyContent: "flex-start",
        width: "100%",
        alignItems: "center"
    },
    lanConatiner: { display: "flex", flexDirection: "row", marginTop: 50 },
    select: {
        height: 40,
        width: "90%",
        marginBottom: 20,
        backgroundColor: "#3b4c69",
        color: "#6780aa",
        borderRadius: 5,
        paddingLeft: 10,
        borderRadius: 5
    },
    selectOuter: {
        width: "60%",
        borderRadius: 5
    }
});


