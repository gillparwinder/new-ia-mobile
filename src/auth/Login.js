import React, { Component } from "react";
import {
    Text,
    View,
    TouchableOpacity,
    TextInput,
    Image,
    StyleSheet,
    Dimensions, Animated, Keyboard, KeyboardAvoidingView
} from "react-native";
const DEVICE_WIDTH = Dimensions.get("window").width;
const DEVICE_HEIGHT = Dimensions.get("window").height;
import Config from "react-native-config";
import DeviceInfo from "react-native-device-info";
async function login(data, header) {
    const url = Config.CORE_ENDPOINT_BASE_URL + "/api/account/login";
    console.log(url + "login");
    let body = {
        user_name: data.user_name,
        password: data.password,
        device_id: 111
        //device_id: DeviceInfo.getUniqueID()
    };
    try {
        let response = await fetch(url, {
            method: "POST",
            headers: header,
            body: JSON.stringify(body)
        });

        return {
            status: response.ok,
            data: await response.json()
        };
    } catch (e) {
        console.error(e);
        throw e;
    }
}
export default class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: "",
            valid: true,
            secureTextEntry: true
        };
        //this.imageHeight = new Animated.Value(100);
    }

    //componentWillMount() {
    //    this.keyboardWillShowSub = Keyboard.addListener('keyboardWillShow', this.keyboardWillShow);
    //    this.keyboardWillHideSub = Keyboard.addListener('keyboardWillHide', this.keyboardWillHide);
    //}

    //keyboardWillShow = (event) => {
    //    Animated.timing(this.imageHeight, {
    //        duration: event.duration,
    //        toValue: 40,
    //    }).start();
    //};

    //keyboardWillHide = (event) => {
    //    Animated.timing(this.imageHeight, {
    //        duration: event.duration,
    //        toValue: 40,
    //    }).start();
    //};
    submit = () => {

        //this.props.reset()
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        //if (this.state.username.length === 0 || this.state.password.length == 0) {
        //    this.setState({ valid: false });
        //} else if (reg.test(this.state.username) === false) {
        //    this.setState({ valid: false });
        //} else {
        //    this.setState({ valid: true });
        //    console.log(this.state.username.toLowerCase())
        this.props.navigation.navigate("HomeStack2")
        //alert(this.state.username.toLowerCase())
        //this.login(this.state.username.toLowerCase(), this.state.password);
        //}
    };

    render() {
        return (
            //<View style={{ flex: 1, backgroundColor: "red" }}>
            <KeyboardAvoidingView style={styles.loginConatiner} behavior="padding" >
                <View style={styles.logoConatiner}>
                    <View>
                        <Image source={require("../assets/logo.png")} resizeMode="contain" style={{ width: 200, height: 200 }} />
                    </View>
                    <View style={{ paddingTop: 30, alignItems: "center" }}>
                        <Text style={styles.loginText}>Login</Text>
                    </View>
                </View>
                <View style={styles.textContainer}>
                    <View style={styles.emailConatiner}>
                        <View>
                            <Image source={require("../assets/mail-icon.png")} resizeMode="contain" style={{ width: 18, height: 18, margin: 10 }} />
                        </View>
                        <View style={styles.emailOuter}>
                            <TextInput
                                style={styles.emailInput}
                                onChangeText={text => this.setState({ username: text })}
                                value={this.state.username}
                                placeholder="Enter Email"
                                placeholderTextColor="silver"
                                keyboardType="email-address" />
                        </View>
                    </View>
                    <View style={styles.passwordConatier}>
                        <View>
                            <Image source={require("../assets/password-icon.png")} resizeMode="contain" style={{ width: 18, height: 18, margin: 10 }} />
                        </View>
                        <View style={[styles.passwordOuter, { position: 'relative' }]}>
                            <TextInput
                                style={styles.passwordInput}
                                onChangeText={text => this.setState({ password: text })}
                                value={this.state.password}
                                placeholder="Enter Password"
                                placeholderTextColor="silver"
                                secureTextEntry={this.state.secureTextEntry}
                                mode="outlined"
                            />
                            <TouchableOpacity
                                onPress={() => this.setState({ secureTextEntry: !this.state.secureTextEntry })}
                                style={{ position: 'absolute', right: 26, top: 8 }}  >
                                {this.state.secureTextEntry ?
                                    (<Image style={{ width: 20, height: 20, resizeMode: "contain" }} source={require('../assets/passwordVisible.png')} />
                                    ) : (
                                        <Image style={{ width: 20, height: 20, resizeMode: "contain" }} source={require('../assets/passwordUnvisible.png')} />
                                    )}
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={styles.forgotContainer} >
                        <TouchableOpacity
                            onPress={() =>
                                //alert("forgot password")
                                this.props.navigation.navigate("ForgotPassword")
                            }>
                            <Text style={styles.forgottext}>Forgotten password?</Text>
                        </TouchableOpacity>
                        {!this.state.valid ? (<Text style={styles.errorText}>Please enter all fields</Text>) : null}

                        {/*{this.props.login_error ? (<Text style={styles.errorText}>{this.props.error_message}</Text>) : null}

            {this.props.invalid_role ? (<Text style={styles.errorText}>Invalid Role</Text>) : null}

            {!this.props.licenceValid ? (<Text style={styles.errorText}> Licence Expired Please contact admin ! </Text>) : null}*/}
                    </View>
                </View>
                <View style={styles.buttonouter}>
                    <TouchableOpacity style={styles.loginButton} onPress={() => this.submit()}>
                        <Text style={styles.text}>Login</Text>
                    </TouchableOpacity>
                </View>
            </KeyboardAvoidingView>
            //</View>
        )
    }
}
const styles = StyleSheet.create({
    buttoncontainer: {
        width: "90%",
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "blue",
        height: 40
    },
    text: {
        textAlign: "center",
        height: 40,
        color: "white",
        paddingTop: 10
    },
    loginConatiner: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "#232d42",
        height: DEVICE_HEIGHT
    },
    resetPasswordSuccessContainer: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "#232d42",
    },
    backToginButton: {
        width: "90%",
        backgroundColor: "#3b4c69",
        marginLeft: 15,
        borderRadius: 5,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center'
    },
    logoConatiner: {
        flex: 2,
        justifyContent: "flex-end",
        display: "flex",
        flexDirection: "column"
    },
    loginText: {
        color: "white",
        fontWeight: "400",
        fontSize: 20
    },
    resetSuccssText: {
        color: "#fff",
        fontWeight: "100",
        fontSize: 16,
        textAlign: 'center'
    },
    textContainer: {
        flex: 2,
        justifyContent: "center",
        width: "100%",
        alignItems: "center"
    },
    emailConatiner: { display: "flex", flexDirection: "row" },
    emailOuter: {
        width: "60%"
    },
    emailInput: {
        height: 40,
        width: "90%",
        marginBottom: 20,
        backgroundColor: "#3b4c69",
        color: "#fff",
        borderRadius: 5,
        paddingLeft: 10
    },
    passwordConatier: { display: "flex", flexDirection: "row" },
    passwordOuter: { width: "60%" },
    passwordInput: {
        height: 40,
        width: "90%",
        // marginBottom: 20,
        backgroundColor: "#3b4c69",
        color: "#fff",
        borderRadius: 5,
        paddingLeft: 10
    },
    forgotContainer: {
        justifyContent: "flex-start",
        flexDirection: "column",
        width: "60%"
    },
    forgottext: {
        textAlign: "left",
        paddingLeft: 20,
        fontStyle: "italic",
        fontSize: 12,
        color: "white",
        paddingTop: 5
    },
    errorText: {
        color: "red",
        fontStyle: "italic",
        fontSize: 12,
        paddingLeft: 20
    },
    buttonouter: { width: "60%", flex: 1 },
    loginButton: {
        width: "90%",
        backgroundColor: "#3b4c69",
        marginLeft: 15,
        borderRadius: 5,
        height: 40
    },
    lanContainer: {
        flex: 2,
        justifyContent: "flex-start",
        width: "100%",
        alignItems: "center"
    },
    lanConatiner: { display: "flex", flexDirection: "row", marginTop: 50 },
    select: {
        height: 40,
        width: "90%",
        marginBottom: 20,
        backgroundColor: "#3b4c69",
        color: "#6780aa",
        borderRadius: 5,
        paddingLeft: 10,
        borderRadius: 5
    },
    selectOuter: {
        width: "60%",
        borderRadius: 5
    }
});