import * as types from "../../../types/types";

export function login(user_name, password) {
  return {
    type: types.LOGIN_REQUEST,
    payload: {
      user_name: user_name,
      password: password
    }
  };
}

export function logout() {
  return {
    type: types.LOGOUT_REQUEST
  };
}

export function reset() {
  return {
    type: types.RESET_ERROR
  };
}

export function forgotPassword(user_name) {
  return {
    type: types.FORGOT_PASSWORD_REQUEST,
    payload: {
      user_name: user_name
    }
  };
}

export function resetPassword(password, confirm_password, access_token) {
  return {
    type: types.RESET_PASSWORD_REQUEST,
    payload: {
      password: password,
      confirm_password: confirm_password,
      access_token: access_token
    }
  };
}

export function resetLogin() {
  return {
    type: types.RESET_LOGIN
  };
}

export function setLanguage(language) {
  return {
    type: types.SET_LANGUAGE_REQUEST,
    payload: {
      language: language
    }
  };
}

export function getMememerEneties() {
  return {
    type: types.GET_MEMBER_ENTITIES_REQUEST
  };
}

export function setMemberEntity(entity, entity_name) {
  return {
    type: types.SET_MEMBER_ENTITIES_REQUEST,
    payload: {
      entity: entity,
      entity_name: entity_name
    }
  };
}

export function validateGuestUser(entity) {
  return {
    type: types.VALIDATE_GUEST_USER_REQUEST,
    payload: {
      entity: entity
    }
  };
}

export function resetAccesToken() {
  return {
    type: types.RESET_ACCESS_TOKEN
  };
}
