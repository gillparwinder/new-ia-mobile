import React, { Component } from "react";
import styles from "./styles";
import {
  Text,
  View,
  TouchableOpacity,
  TextInput,
  Image,
  Dimensions,
  KeyboardAvoidingView,
  ScrollView,
  Keyboard,
  ActivityIndicator
} from "react-native";
import { login, reset } from "../actions/login";
import { connect } from "react-redux";
import { Icon } from "react-native-elements";
import { resetPassword, logout } from '../actions/login'

class PasswordReset extends Component {

  constructor(props) {
    super(props);
    this.state = {
      oldPassword: "",
      newPassword: "",
      confirmPassword: "",
      isError: false,
      errorMessage: ""
    };
  }

  onSubmit = () => {
    if (this.state.newPassword.length == 0 || this.state.confirmPassword.length == 0)
      this.setState({ isError: true, errorMessage: "*please fill in all the fields" });
    else if (this.state.newPassword != this.state.confirmPassword)
      this.setState({ isError: true, errorMessage: "*new password and confirm password do not match." });
    else {
      this.setState({ isError: false, errorMessage: "" });
      Keyboard.dismiss();
      this.props.resetPassword(this.state.newPassword, this.state.confirmPassword, this.props.access_token);
    }
  }

  componentDidUpdate(nextProps) {
    if (nextProps.passwordResetRequestSuccess != this.props.passwordResetRequestSuccess && this.props.passwordResetRequestSuccess) {
      this.props.logout();
    }
    if (nextProps.passwordResetRequestFailed != this.props.passwordResetRequestFailed && this.props.passwordResetRequestFailed) {
      this.setState({ isError: true, errorMessage: "*something went wrong in resetting password." });
    }
  }

  render() {
    return (
      <ScrollView keyboardShouldPersistTaps={true}>
        <KeyboardAvoidingView style={styles.loginConatiner} behavior="padding">
          <View style={styles.logoConatiner}>
            <View>
              <Image source={require("./../../../assests/images/logo.png")} resizeMode="contain" style={{ width: 200, height: 100 }} />
            </View>
            <View style={{ paddingTop: 30, alignItems: "center", marginBottom: 15 }}>
              <Text style={styles.loginText}> Password reset </Text>
            </View>
          </View>
          <View style={styles.textContainer}>
            {/* <View style={styles.emailConatiner}>
              <View style={{ paddingLeft: 10, paddingRight: 10, paddingTop: 5 }} >
                <Icon name="lock" type="font-awesome" color="#fff" size={25} />
              </View>
              <View style={styles.emailOuter}>
                <TextInput style={styles.emailInput}
                  secureTextEntry password={true}
                  onChangeText={text => this.setState({ oldPassword: text })}
                  value={this.state.oldPassword}
                  placeholder="Old password"
                />
              </View>
            </View> */}

            <View style={styles.emailConatiner}>
              <View>
                <Image source={require("./../../../assests/images/password-icon.png")} resizeMode="contain" style={{ width: 18, height: 18, margin: 10 }} />
              </View>
              <View style={styles.emailOuter}>
                <TextInput style={[styles.passwordInput, { marginBottom: 15 }]} placeholder="New password" secureTextEntry password={true}
                  onChangeText={text => this.setState({ newPassword: text })}
                  value={this.state.newPassword}
                />
              </View>
            </View>

            <View style={styles.passwordConatier}>
              <View>
                <Image
                  source={require("./../../../assests/images/password-icon.png")}
                  resizeMode="contain"
                  style={{ width: 18, height: 18, margin: 10 }}
                />
              </View>
              <View style={styles.passwordOuter}>
                <TextInput
                  style={styles.passwordInput}
                  onChangeText={text => this.setState({ confirmPassword: text })}
                  value={this.state.confirmPassword}
                  placeholder="Confirm new password"
                  secureTextEntry
                  password={true}
                />
              </View>
            </View>
          </View>
          <View style={[styles.buttonouter, { marginTop: 15, marginLeft: 5 }]}>
            {this.state.isError ?
              <Text style={{ color: 'red', marginBottom: 10, textAlign: 'center' }}>{this.state.errorMessage}</Text> : null
            }
            <TouchableOpacity
              style={styles.loginButton}
              onPress={() => this.onSubmit()}
            >
              <Text style={styles.text}>Password reset</Text>
            </TouchableOpacity>
          </View>

          {
            this.props.processingRequest ?
              <View style={{ position: 'absolute', zIndex: 100, height: '100%', width: '100%', justifyContent: "center", alignItems: "center" }} >
                <ActivityIndicator size="large" color="#0000ff" />
              </View>
              : null
          }
        </KeyboardAvoidingView>
      </ScrollView>
    )
  }
}

const mapStateToProps = state => {
  return {
    loggedIn: state.loginReducer.loggedIn,
    passwordResetRequestSuccess: state.loginReducer.passwordResetRequestSuccess,
    passwordResetRequestFailed: state.loginReducer.passwordResetRequestFailed,
    processingRequest: state.loginReducer.processingRequest,
    access_token: state.loginReducer.access_token
  };
};

const mapDispatchToProps = {
  resetPassword,
  logout
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PasswordReset);