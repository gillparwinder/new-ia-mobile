/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import 'react-native-gesture-handler';
import React from 'react';
import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from "@react-navigation/native";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { ScrollView, TouchableOpacity, Text, StyleSheet, View, Image } from 'react-native';
import Login from './src/auth/Login';
import ForgotPassword from './src/auth/ForgotPassword';
import Language from './src/components/Language';
import Home from './src/components/Home';
import PasswordReset from './src/components/PasswordReset';

const navOptionHandler = () => ({
  headerShown: false
});


const Drawer = createDrawerNavigator();
function DrawerNavigator() {
  return (
    <Drawer.Navigator drawerPosition={"right"} statusBarAnimation="slide"
      //initialRouteName="Dashboard"
      drawerContent={props =>
        <View style={{ width: "100%", height: "100%", backgroundColor: "transparent" }}>
          <Image style={{ height: "30%", width: "100%", resizeMode: "cover", alignSelf: "center", position: "absolute" }} source={require("./src/assets/bg.jpg")} />
          <View square style={{ height: "30%", width: "100%", justifyContent: "center", alignItems: "center" }} >
            <Image style={{ width: "40%", height: 200, alignSelf: "center", resizeMode: "contain" }} source={require("./src/assets/logoTransparent.png")} />
          </View>

          <ScrollView style={{ marginLeft: 5 }}>
            <TouchableOpacity
              style={Styles.button}
              onPress={() => {
                props.navigation.navigate("Language");
              }}
            >
              <View style={{ justifyContent: "center", alignItems: "center", flexDirection: "row" }}>
                {/*<Thumbnail square style={{ width: 25, height: 25, resizeMode: "contain" }} source={require('./src/assets/home.png')} ></Thumbnail>*/}
                <Text style={Styles.text}>Language Settings</Text>
              </View>
              {/*<Thumbnail square style={{ width: 20, height: 20, resizeMode: "contain", marginRight: 10 }} source={require('./src/assets/rightArrow.png')} ></Thumbnail>*/}
            </TouchableOpacity>
            <TouchableOpacity
              style={Styles.button}
              onPress={() => {
                props.navigation.navigate("PasswordReset")
                //props.navigation.navigate("Landing");
              }}
            >
              <View style={{ justifyContent: "center", alignItems: "center", flexDirection: "row" }}>
                {/*<Thumbnail square style={{ width: 25, height: 25, resizeMode: "contain" }} source={require('./src/assets/user.png')} ></Thumbnail>*/}

                <Text style={Styles.text}>Reset Password</Text>
              </View>
              {/*<Thumbnail small style={{ width: 20, height: 20, resizeMode: "contain", marginRight: 10 }} source={require('./src/assets/rightArrow.png')} ></Thumbnail>*/}
            </TouchableOpacity>

          </ScrollView>
          <TouchableOpacity style={{ marginBottom: 40, marginLeft: 20 }}
            onPress={() => {
              props.navigation.navigate("Login")
            }}>
            <Text style={{ fontWeight: "bold", color: "red", fontSize: 18 }}>Logout </Text>
          </TouchableOpacity>
        </View>
      }
    //  drawerContent={() => <CustomDrawerContent/>}
    >
      {/*<Drawer.Screen name="Landing" component={Landing} />*/}
      <Drawer.Screen name="Home" component={Home} />
      <Drawer.Screen name="Language" component={Language} />
      <Drawer.Screen name="PasswordReset" component={PasswordReset} />
    </Drawer.Navigator>
  );
}
const StackHome = createStackNavigator();
function HomeStack() {
  return (
    <StackHome.Navigator initialRouteName="Login">
      <StackHome.Screen
        name="Login"
        component={Login}
        options={navOptionHandler}
      />
      <StackHome.Screen
        name="ForgotPassword"
        component={ForgotPassword}
        options={navOptionHandler}
      />
    </StackHome.Navigator>
  );
}

const StackHome2 = createStackNavigator();
function HomeStack2() {
  return (
    <StackHome2.Navigator mode="modal" initialRouteName="Drawer">
      {/*<StackHome2.Screen
        name="Language"
        component={Language}
        options={navOptionHandler}
      />*/}
      <StackHome2.Screen
        name="Drawer"
        component={DrawerNavigator}
        options={navOptionHandler}
      />
    </StackHome2.Navigator>
  );
}



const StackApp = createStackNavigator();
export default function App() {
  return (
    <NavigationContainer>
      <StackApp.Navigator mode="modal" initialRouteName="HomeApp">
        <StackApp.Screen
          name="HomeApp"
          component={HomeStack}
          options={navOptionHandler}
        />
        <StackApp.Screen
          name="HomeStack2"
          component={HomeStack2}
          options={navOptionHandler}
        />
      </StackApp.Navigator>
    </NavigationContainer>
  )
};
const Styles = StyleSheet.create({
  button: {
    width: '98%',
    alignSelf: "center",
    paddingVertical: 8,
    marginTop: 10,
    marginLeft: 10,
    //backgroundColor: '#EFEFEF',
    flexDirection: 'row',
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: "space-between"
  },
  text: {
    fontWeight: "bold",
    marginLeft: 20
  }
})
