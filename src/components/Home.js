import React, { Component } from "react";
import {
    View,
    Text,
    TouchableOpacity,
    TextInput,
    Image,
    TouchableHighlight,
    StyleSheet,
    Dimensions,
    ImageBackground,
    ActivityIndicator,
    Modal
} from "react-native";
import { Picker } from '@react-native-community/picker';
import { SafeAreaView } from "react-native";
const Item = Picker.Item;
const DEVICE_WIDTH = Dimensions.get("window").width;
const DEVICE_HEIGHT = Dimensions.get("window").height;
//const languages = require("../assets/languages.json");
export default class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            MenuAction: "", loader: false
        };
    }

    componentDidMount() {
        //this.setState({
        //    language: this.props.language
        //});
    }

    submit = () => {
        //this.props.setLanguage(this.state.language);
        //if (this.props.role == "GUEST_USER") {
        //  this.props.navigation.navigate("MemberEntity");
        //}
        //else if (this.props.role == "CENTRAL_SUPPORT_EXECUTIVE") {
        //  this.props.navigation.navigate("ListFieldExecutives");
        //}
        //else 
        //this.props.navigation.navigate("Home");
        alert("home")
    };

    render() {
        return (
            <ImageBackground
                source={require('../assets/customer-support-banner.jpg')}
                resizeMode='cover'
                style={styles.imgBackground}
            >

                <SafeAreaView>
                    <View style={{ width: "92%", alignSelf: "center", height: 40, flexDirection: "row", justifyContent: "space-between", alignItems: "flex-end" }}>
                        {/*{!this.state.PickerVisible ?*/}
                        <Image source={require('../assets/logoTransparent.png')} style={{ width: 35, height: 35, resizeMode: "contain" }} ></Image>
                        <TouchableOpacity onPress={() => {
                            this.props.navigation.openDrawer()
                        }}
                        >
                            <Image source={require('../assets/menu.png')} style={{ width: 25, height: 25, resizeMode: "contain", }} ></Image>
                        </TouchableOpacity>
                        {/*</View>:
                            <Picker
                                textStyle={{ color: "#6780aa", width: "100%" }}
                                style={{ color: "#6780aa" }}
                                mode="dialog"
                                selectedValue={this.state.MenuAction}
                                prompt="Select"
                                onValueChange={value => this.setState({ MenuAction: value, PickerVisible: false })}
                                itemStyle={{ backgroundColor: "#eee" }}
                            >
                                <Picker.Item label="Language Setting" value="Language Setting" />
                                <Picker.Item label="Reset Password" value="Reset Password" />
                                <Picker.Item label="Logout" value="Logout" />

                            </Picker>}*/}
                    </View>
                    <View style={styles.callContainer}>
                        {/* <View>
            <Image
              source={require('../../../../assests/images/call.png')}
              resizeMode='contain'
              style={styles.callImag}
            />
          </View> */}
                        {!this.state.loader ? (
                            <View>
                                {/*{this.props.feature_config.allow_call ?*/}
                                <TouchableOpacity style={styles.callTextOuter}
                                    onPress={() => {
                                        this.setState({ loader: true })
                                    }}
                                    //onPress={debounce(() => {
                                    //    if (!this.props.sessionRequest) {
                                    //        this.setState({ loader: true })
                                    //        this.connectSession()
                                    //    }
                                    //}, 1000)} 
                                    disabled={this.state.loader}  >
                                    <Image
                                        source={require('../assets/iconcall.png')}
                                        resizeMode='contain'
                                        style={styles.callIconImag}
                                    />
                                    <Text style={{ marginLeft: 20, fontWeight: 'bold', fontSize: 16, color: '#242E42', textTransform: "uppercase" }}>
                                        Call customer support
              </Text>
                                </TouchableOpacity>
                                {/*: null}*/}

                                {/*{this.props.feature_config.allow_record ?*/}
                                <TouchableOpacity style={[styles.callTextOuter, { marginTop: 30 }]} onPress={() => { this.props.navigation.navigate("VideoRecord") }} >
                                    <Image style={styles.callIconImag} source={require('../assets/iconcall.png')} />
                                    <Text style={{ marginLeft: 20, fontWeight: 'bold', fontSize: 16, color: '#242E42', textTransform: "uppercase" }}>
                                        start Video Recording
                </Text>
                                </TouchableOpacity>
                                {/*: null}*/}
                            </View>

                        ) : (
                                <View style={[styles.callTextOuter, { minWidth: 250, justifyContent: 'center', alignItems: 'center' }]}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Text
                                            style={{ fontWeight: '400', fontSize: 18, color: '#000' }}
                                        >
                                            Calling ...
                </Text>
                                        <ActivityIndicator size='small' color='#0000ff' />
                                    </View>
                                </View>
                            )}
                    </View>
                    <View style={{ position: 'absolute', bottom: 10, width: '100%' }}>
                        <View style={styles.inforow}>
                            <Text style={styles.infoLabel}>Username : </Text>
                            {/*<Text style={styles.infoValue}>{this.props.user_name}</Text>*/}
                        </View>
                        <View style={styles.inforow}>
                            <Text style={styles.infoLabel}>Client : </Text>
                            <Text style={styles.infoValue}>
                                {/*{this.props.entity_name}*/}
                            </Text>
                        </View>
                        {this.props.role == "GUEST_USER" ?
                            <View style={{ alignItems: 'center' }}>
                                <TouchableOpacity onPress={() => { this.props.navigation.navigate("MemberEntity") }} style={styles.btnChangeClient} >
                                    <Text style={{ fontWeight: '400', fontSize: 14, color: '#000' }}>Change Client</Text>
                                </TouchableOpacity>
                            </View>
                            : null}
                    </View>
                </SafeAreaView>
            </ImageBackground>

        );
    }
}
const styles = StyleSheet.create({
    buttoncontainer: {
        width: "90%",
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "blue",
        height: 40
    },

    text: {
        textAlign: "center",
        height: 40,
        color: "white",
        paddingTop: 10
    },
    loginConatiner: {
        display: "flex",
        flexDirection: "column",
        //alignItems: "center",
        //justifyContent: "center",
        backgroundColor: "#1359C8",
        height: DEVICE_HEIGHT
    },
    resetPasswordSuccessContainer: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "#232d42",
    },
    backToginButton: {
        width: "90%",
        backgroundColor: "#3b4c69",
        marginLeft: 15,
        borderRadius: 5,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center'
    },
    logoConatiner: {
        flex: 2,
        justifyContent: "flex-end",
        display: "flex",
        flexDirection: "column"
    },
    loginText: {
        color: "white",
        fontWeight: "400",
        fontSize: 20
    },
    resetSuccssText: {
        color: "#fff",
        fontWeight: "100",
        fontSize: 16,
        textAlign: 'center'
    },
    textContainer: {
        flex: 2,
        justifyContent: "center",
        width: "100%",
        alignItems: "center"
    },
    emailConatiner: { display: "flex", flexDirection: "row" },
    emailOuter: {
        width: "60%"
    },
    emailInput: {
        height: 40,
        width: "90%",
        marginBottom: 20,
        backgroundColor: "#3b4c69",
        color: "#fff",
        borderRadius: 5,
        paddingLeft: 10
    },
    passwordConatier: { display: "flex", flexDirection: "row" },
    passwordOuter: { width: "60%" },
    passwordInput: {
        height: 40,
        width: "90%",
        // marginBottom: 20,
        backgroundColor: "#3b4c69",
        color: "#fff",
        borderRadius: 5,
        paddingLeft: 10
    },
    forgotContainer: {
        justifyContent: "flex-start",
        flexDirection: "column",
        width: "60%"
    },
    forgottext: {
        textAlign: "left",
        paddingLeft: 20,
        fontStyle: "italic",
        fontSize: 12,
        color: "white",
        paddingTop: 5
    },
    errorText: {
        color: "red",
        fontStyle: "italic",
        fontSize: 12,
        paddingLeft: 20
    },
    buttonouter: { width: "60%", flex: 1 },
    loginButton: {
        width: "90%",
        backgroundColor: "#3b4c69",
        marginLeft: 15,
        borderRadius: 5,
        height: 40
    },
    lanContainer: {
        flex: 2,
        justifyContent: "flex-start",
        width: "100%",
        alignItems: "center"
    },
    lanConatiner: { display: "flex", flexDirection: "row", marginTop: 50 },
    select: {
        height: 40,
        width: "90%",
        marginBottom: 20,
        backgroundColor: "#3b4c69",
        color: "#6780aa",
        borderRadius: 5,
        paddingLeft: 10,
        borderRadius: 5
    },
    selectOuter: {
        width: "60%",
        borderRadius: 5
    },
    imgBackground: {
        display: 'flex',
        flexDirection: 'column',
        flex: 1
    },
    publisherContainerPotrait: { flex: 1 },
    publisherContainerLandscape: {},
    messageConatiner: {
        width: DEVICE_WIDTH,
        height: 200,
        position: 'absolute',
        bottom: 0,
        borderTopColor: '#eee',
        borderTopWidth: 2,
        display: 'flex',
        flexDirection: 'column',
        backgroundColor: '#fff'
    },
    bubbleOuter: {
        width: DEVICE_WIDTH,
        display: 'flex'
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF'
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5
    },
    bubbleInner: {
        width: DEVICE_WIDTH - 50,
        borderRadius: 50,
        borderWidth: 2,
        borderColor: '#eee',
        margin: 5,
        marginRight: 20
    },
    messageFooterContainer: {
        width: DEVICE_WIDTH,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fff'
    },
    sendBubble: {
        width: DEVICE_WIDTH - 20,
        borderRadius: 50,
        borderWidth: 2,
        borderColor: '#2196f3',
        margin: 10,
        marginTop: 20,
        marginBottom: 20,
        backgroundColor: '#fff',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        height: 40
    },
    screenshotBundle: {
        width: DEVICE_WIDTH - 20,
        position: 'relative',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        flexDirection: 'row',
        borderWidth: 2,
        borderColor: '#2196f3',
        marginBottom: 20,
        padding: 10,
        borderRadius: 10,
    },
    screenShotButtonWrap: {
        marginLeft: 10,

    },
    cancelScreenshotButton: {
        backgroundColor: 'red',
        width: 40,
        height: 40,
        borderRadius: 40,
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 10
    },
    sendScreenshotButton: {
        backgroundColor: '#2196f3',
        width: 40,
        height: 40,
        borderRadius: 40,
        alignItems: 'center',
        justifyContent: 'center',
        // position: "absolute",
        // right: 10,
        // bottom: 10
    },
    sendButton: {
        // borderWidth: 1,
        // backgroundColor: "#2196f3",
        // borderColor: "rgba(0,0,0,0.2)",
        alignItems: 'center',
        justifyContent: 'center',
        // height: 40,
        // width: 70,
        // borderRadius: 50,
        flex: 1
    },
    backgroundImg: {
        // flex: 1,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        width: '100%'
    },
    backgroundI: { width: 40, height: 40, margin: 20 },
    backgroundIe: {
        width: 40,
        height: 40,
        position: 'absolute',
        right: 0,
        margin: 20
    },
    callContainer: {
        height: "90%",
        justifyContent: 'center',
        alignItems: 'center',
        display: 'flex',
        flexDirection: 'column',
        //marginBottom: '30%',
        //backgroundColor: '#ccc'
    },
    callImag: { width: 60, height: 60, margin: 20 },
    callIconImag: { width: 30, height: 30 },
    callTextOuter: {
        // width: '100%',
        paddingLeft: 10,
        paddingRight: 10,
        flexDirection: 'row',
        // justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#fff',
        height: 50,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#79BD4B',
        marginTop: 40,
        shadowColor: '#79BD4B',
        shadowOffset: {
            width: 4,
            height: 6
        },
        shadowRadius: 2.81,
        elevation: 8
    },
    driverViewContainer: {
        flexDirection: 'row',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#eee'
    },
    modalContainer: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    modalOuter: {
        width: '90%',
        backgroundColor: '#fff',
        display: 'flex',
        flexDirection: 'column',
        borderRadius: 4
    },
    settings: {
        flex: 1,
        padding: 10,
        width: '100%'
    },
    messageModal: {
        height: 250,
        position: 'absolute',
        bottom: 60,
        width: DEVICE_WIDTH
        // backgroundColor: "#fff"
    },
    messageModalcontainer: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%'
    },
    messagemodalOuter: {
        margin: 10,
        padding: 10,
        display: 'flex',
        flexDirection: 'column',
        borderRadius: 4,
        zIndex: 9999
    },
    messageModalout: { position: 'absolute', bottom: 0 },
    topLine: {
        borderBottomWidth: 1,
        borderBottomColor: '#9e9e9e',
        // position: "absolute",
        height: 10,
        // backgroundColor: 'transparent',
        width: '100%'
    },
    closeMessage: { position: 'absolute', left: 20, top: 0, zIndex: 999 },
    closechat: { position: 'absolute', right: 20, top: 0, zIndex: 999 },
    closeBotton: {
        width: 20,
        height: 20,
        backgroundColor: '#2196f3',
        borderRadius: 50,
        alignItems: 'center',
        justifyContent: 'center'
    },
    expandeMessage: { position: 'absolute', left: 20, top: -5, zIndex: 999 },
    expandeButton: {
        width: 20,
        height: 20,
        backgroundColor: '#9e9e9e',
        borderRadius: 50,
        alignItems: 'center',
        justifyContent: 'center'
    },
    bubbleTop: { zIndex: 1, top: 10, backgroundColor: '#fff', height: '100%' },
    inputOuter: { padding: 10, color: '#000', flex: 5 },
    borderGrey: { borderTopColor: '#9e9e9e' },
    borderWhite: { borderTopColor: '#fff' },
    sendBubble2: {
        width: DEVICE_WIDTH - 30,
        borderRadius: 50,
        borderWidth: 2,
        borderColor: '#2196f3',
        backgroundColor: '#fff',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        height: 40,
        marginBottom: 5,
        marginLeft: 10
    },

    clientName: {
        backgroundColor: '#000',
        padding: 10
    },
    txtClient: {
        color: '#fff'
    },
    clientsWrap: {
        backgroundColor: '#010101',
        padding: 10
    },
    selectClient: {
        borderBottomColor: '#fff',
        borderBottomWidth: 1,
        paddingBottom: 10,
        paddingTop: 10
    },

    bottomButtonOuter: {
        position: 'absolute',
        width: '100%',
        bottom: 10,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },

    btnCallEnd: {
        marginLeft: 15,
        marginRight: 15,
        width: 35,
        height: 35,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'red',
        borderRadius: 25
    },
    btnMuteAudio: {
        marginLeft: 15,
        marginRight: 15,
        width: 35,
        height: 35,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 25
    },
    btnMuteVideo: {
        marginLeft: 15,
        marginRight: 15,
        width: 35,
        height: 35,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 25
    },
    btnActive: {
        backgroundColor: 'green',
    },
    btnInActive: {
        backgroundColor: '#ccc',
    },

    btnShowMessage: {
        width: 35,
        height: 35,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'blue',
        borderRadius: 25
    },
    inforow: {
        flexDirection: 'row',
        justifyContent: 'center'
    },
    infoLabel: {
        color: '#ebecf0',
        fontSize: 16,
        fontStyle: 'italic'
    },
    btnChangeClient: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 30,
        width: 120,
        marginTop: 10,
        paddingLeft: 10,
        paddingRight: 10,
        backgroundColor: '#fff',
        borderRadius: 5,
        shadowColor: '#E0E0E0',
        shadowOffset: {
            width: 4,
            height: 6
        },
        shadowRadius: 2.81,
        elevation: 8
    },
    infoValue: {
        color: '#ebecf0',
        fontSize: 16,
    }
});


