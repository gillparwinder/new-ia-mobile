import React, { Component } from "react";
import styles from "./styles";
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  Image,
  TouchableHighlight,
  Picker,
  Modal
} from "react-native";
import {
  getMememerEneties,
  setMemberEntity,
  logout,
  resetAccesToken
} from "../actions/login";
import { connect } from "react-redux";
import { Icon, Button } from "react-native-elements";
const Item = Picker.Item;

const memberEntities = [
  {
    valid_from: "2019-11-12T00:00:00.000Z",
    valid_until: "2019-11-13T00:00:00.000Z",
    name: "Marvel Inc",
    entity_id: "5dca93716291ee6fd93561c8"
  },
  {
    valid_from: "2019-11-11T00:00:00.000Z",
    valid_until: "2019-11-13T00:00:00.000Z",
    name: "hari",
    entity_id: "5dca61ad5483284e25056541"
  }
];

class MemberEntity extends Component {
  constructor(props) {
    super(props);
    this.state = {
      entity_id: "",
      entity_name: "",
      memberEntities: [],
      error: false
    };
  }

  componentDidMount() {
    this.props.getMememerEneties();
  }

  componentDidUpdate(nextProps) {
    if (this.props.member_entities != nextProps.member_entities) {
      if (this.props.member_entities.length == 0) {
        this.setState({
          error: true
        });
      } else {
        this.setState({
          error: false
        });
      }
    }
  }

  submit = () => {
    this.props.setMemberEntity(this.state.entity_id, this.state.entity_name);
    this.props.resetAccesToken();
    this.props.navigation.navigate("Home");
  };

  render() {
    return (
      <View style={styles.loginConatiner}>
        <View style={styles.logoConatiner}>
          <View>
            <Image
              source={require("./../../../assests/images/logo.png")}
              resizeMode="contain"
              style={{ width: 200, height: 100 }}
            />
          </View>
          <View
            style={{
              paddingTop: 30,
              alignItems: "center",
              paddingBottom: 30
              // width: "100%"
            }}
          >
            {this.state.error ? (
              <Text
                style={{
                  color: "#fff",
                  fontWeight: "100",
                  fontSize: 18,
                  width: 200,
                  textAlign: "center"
                }}
              >
                Your licence has expired please contact admin
              </Text>
            ) : (
                <Text style={styles.loginText}>Select Member Entity</Text>
              )}
          </View>
        </View>
        <View style={{ flex: 2, alignItems: "center" }}>
          {this.props.member_entities.map((item, index) => {
            return (
              <TouchableOpacity
                key={index}
                style={{
                  paddingLeft: 20,
                  paddingRight: 20,
                  paddingTop: 10,
                  paddingBottom: 10,
                  alignItems: "center",
                  width: 200,
                  backgroundColor: "#3b4c69",
                  marginLeft: 15,
                  borderRadius: 5,
                  height: 40,
                  margin: 5,
                  flexDirection: "row",
                  justifyContent: "center"
                }}
                onPress={() => this.setState({ entity_id: item.entity_id, entity_name: item.name })}
              >
                <Text style={{ color: "#fff", textAlign: "center" }}>
                  {item.name}
                </Text>
                {this.state.entity_id == item.entity_id ? (
                  <Image
                    source={require("./../../../assests/images/ok.png")}
                    resizeMode="contain"
                    style={{
                      width: 20,
                      height: 20,
                      position: "absolute",
                      right: 10
                    }}
                  ></Image>
                ) : null}
              </TouchableOpacity>
            );
          })}
        </View>
        <View style={styles.buttonouter}>
          {this.state.entity_id != "" ? (
            <TouchableOpacity
              style={styles.loginButton}
              onPress={() => this.submit()}
            >
              <Text style={styles.text}>Save</Text>
            </TouchableOpacity>
          ) : (
              <TouchableOpacity
                style={styles.loginButton}
                onPress={() => this.props.logout()}
              >
                <Text style={styles.text}>Logout</Text>
              </TouchableOpacity>
            )}
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    member_entities: state.loginReducer.member_entities,
    role: state.loginReducer.role
  };
};

const mapDispatchToProps = {
  getMememerEneties,
  setMemberEntity,
  logout,
  resetAccesToken
};

export default connect(mapStateToProps, mapDispatchToProps)(MemberEntity);
