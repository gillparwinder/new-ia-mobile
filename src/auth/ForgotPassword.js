import React, { Component } from "react";
import {
    Text,
    View,
    TouchableOpacity,
    TextInput,
    Image,
    StyleSheet,
    Dimensions, Animated, Keyboard, KeyboardAvoidingView,
    ActivityIndicator
} from "react-native";
const DEVICE_WIDTH = Dimensions.get("window").width;
const DEVICE_HEIGHT = Dimensions.get("window").height;

export default class ForgotPassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user_name: "",
            valid: true,
            resetSuccess: false,
            resetFailed: false
        };
        //this.imageHeight = new Animated.Value(100);
    }

    ;

    submit = () => {
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (this.state.user_name.length === 0) {
            this.setState({ valid: false });
        } else if (reg.test(this.state.user_name) === false) {
            this.setState({ valid: false });
        } else {
            this.setState({ valid: true });
            Keyboard.dismiss();
            this.props.navigation.goBack()
            //  this.props.forgotPassword(this.state.user_name);
        }
    };

    render() {
        return (
            this.state.resetSuccess ?
                <View style={styles.resetPasswordSuccessContainer}>
                    <View>
                        <Animated.Image source={require("../assets/logo.png")} resizeMode="contain" style={{ width: 200, height: 200 }} />
                    </View>
                    <View style={{ padding: 30, alignItems: "center" }}>
                        <Text style={styles.resetSuccssText}>Please check registered Email Id or Mobile Number to continue.</Text>
                    </View>
                    <View style={styles.forgotContainer}>
                        <TouchableOpacity style={styles.backToginButton} onPress={() =>
                            this.props.navigation.navigate("Login")}
                        >
                            <Text style={{ textAlign: "center", height: 40, color: "white", paddingTop: 10 }}>Back to Login</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                :
                <KeyboardAvoidingView style={styles.loginConatiner} behavior="padding">
                    <View style={styles.logoConatiner}>
                        <View>
                            <Animated.Image source={require("../assets/logo.png")} resizeMode="contain" style={{ width: 200, height: 200 }} />
                        </View>
                        <View style={{ paddingTop: 30, alignItems: "center" }}>
                            <Text style={styles.loginText}>ForgotPassword</Text>
                        </View>
                    </View>
                    <View style={styles.textContainer}>
                        <View style={styles.emailConatiner}>
                            <View>
                                <Image source={require("../assets/mail-icon.png")} resizeMode="contain" style={{ width: 18, height: 18, margin: 10 }} />
                            </View>
                            <View style={styles.emailOuter}>
                                <TextInput
                                    style={styles.emailInput}
                                    onChangeText={text => this.setState({ user_name: text })}
                                    value={this.state.user_name}
                                    placeholder="Enter Email"
                                    placeholderTextColor="silver"
                                    keyboardType="email-address"
                                />
                                {!this.state.valid ? <Text style={{ color: 'red', marginBottom: 5 }}>*enter valid email id</Text> : null}
                                {this.state.resetFailed ? <Text style={{ color: 'red', marginBottom: 5 }}>something went wrong!</Text> : null}
                            </View>
                        </View>
                        <View style={styles.forgotContainer}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate("Login")}>
                                <Text style={styles.forgottext}>back to login</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={styles.buttonouter}>
                        <TouchableOpacity style={styles.loginButton} onPress={() => this.submit()}>
                            <Text style={styles.text}>Submit</Text>
                        </TouchableOpacity>
                    </View>
                    {
                        this.props.processingRequest ?
                            <View style={{ position: 'absolute', zIndex: 100, height: '100%', width: '100%', justifyContent: "center", alignItems: "center" }} >
                                <ActivityIndicator size="large" color="#0000ff" />
                            </View>
                            : null
                    }
                </KeyboardAvoidingView>
        );
    }
}
const styles = StyleSheet.create({
    buttoncontainer: {
        width: "90%",
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "blue",
        height: 40
    },
    text: {
        textAlign: "center",
        height: 40,
        color: "white",
        paddingTop: 10
    },
    loginConatiner: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "#232d42",
        height: DEVICE_HEIGHT
    },
    resetPasswordSuccessContainer: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "#232d42",
    },
    backToginButton: {
        width: "90%",
        backgroundColor: "#3b4c69",
        marginLeft: 15,
        borderRadius: 5,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center'
    },
    logoConatiner: {
        flex: 2,
        justifyContent: "flex-end",
        display: "flex",
        flexDirection: "column"
    },
    loginText: {
        color: "white",
        fontWeight: "400",
        fontSize: 20
    },
    resetSuccssText: {
        color: "#fff",
        fontWeight: "100",
        fontSize: 16,
        textAlign: 'center'
    },
    textContainer: {
        flex: 2,
        justifyContent: "center",
        width: "100%",
        alignItems: "center"
    },
    emailConatiner: { display: "flex", flexDirection: "row" },
    emailOuter: {
        width: "60%"
    },
    emailInput: {
        height: 40,
        width: "90%",
        marginBottom: 20,
        backgroundColor: "#3b4c69",
        color: "#fff",
        borderRadius: 5,
        paddingLeft: 10
    },
    passwordConatier: { display: "flex", flexDirection: "row" },
    passwordOuter: { width: "60%" },
    passwordInput: {
        height: 40,
        width: "90%",
        // marginBottom: 20,
        backgroundColor: "#3b4c69",
        color: "#fff",
        borderRadius: 5,
        paddingLeft: 10
    },
    forgotContainer: {
        justifyContent: "flex-start",
        flexDirection: "column",
        width: "60%"
    },
    forgottext: {
        textAlign: "left",
        paddingLeft: 20,
        fontStyle: "italic",
        fontSize: 12,
        color: "white",
        paddingTop: 5
    },
    errorText: {
        color: "red",
        fontStyle: "italic",
        fontSize: 12,
        paddingLeft: 20
    },
    buttonouter: { width: "60%", flex: 1 },
    loginButton: {
        width: "90%",
        backgroundColor: "#3b4c69",
        marginLeft: 15,
        borderRadius: 5,
        height: 40
    },
    lanContainer: {
        flex: 2,
        justifyContent: "flex-start",
        width: "100%",
        alignItems: "center"
    },
    lanConatiner: { display: "flex", flexDirection: "row", marginTop: 50 },
    select: {
        height: 40,
        width: "90%",
        marginBottom: 20,
        backgroundColor: "#3b4c69",
        color: "#6780aa",
        borderRadius: 5,
        paddingLeft: 10,
        borderRadius: 5
    },
    selectOuter: {
        width: "60%",
        borderRadius: 5
    }
});
