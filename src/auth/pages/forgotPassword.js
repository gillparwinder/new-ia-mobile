import React, { Component } from "react";
import styles from "./styles";
import {
  Text,
  View,
  TouchableOpacity,
  TextInput,
  Image,
  Dimensions, Animated, Keyboard, KeyboardAvoidingView,
  ActivityIndicator
} from "react-native";
import { forgotPassword } from "../actions/login";
import { connect } from "react-redux";
const DEVICE_WIDTH = Dimensions.get("window").width;
const DEVICE_HEIGHT = Dimensions.get("window").height;

class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user_name: "",
      valid: true,
      resetSuccess: false,
      resetFailed: false
    };
    this.imageHeight = new Animated.Value(100);
  }

  componentDidUpdate(nextProps) {
    if (nextProps.forgotPasswordRequestSuccess != this.props.forgotPasswordRequestSuccess && this.props.forgotPasswordRequestSuccess) {
      this.setState({ resetSuccess: true });
    }
    if (nextProps.forgotPasswordRequestFailed != this.props.forgotPasswordRequestFailed && this.props.forgotPasswordRequestFailed) {
      this.setState({ resetFailed: true });
    }
  }

  componentWillMount() {
    this.keyboardWillShowSub = Keyboard.addListener('keyboardWillShow', this.keyboardWillShow);
    this.keyboardWillHideSub = Keyboard.addListener('keyboardWillHide', this.keyboardWillHide);
  }

  // componentWillUnmount() {
  //   this.keyboardWillShowSub.remove();
  //   this.keyboardWillHideSub.remove();
  // }

  keyboardWillShow = (event) => {
    Animated.timing(this.imageHeight, {
      duration: event.duration,
      toValue: 40,
    }).start();
  };

  keyboardWillHide = (event) => {
    Animated.timing(this.imageHeight, {
      duration: event.duration,
      toValue: 40,
    }).start();
  };

  submit = () => {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (this.state.user_name.length === 0) {
      this.setState({ valid: false });
    } else if (reg.test(this.state.user_name) === false) {
      this.setState({ valid: false });
    } else {
      this.setState({ valid: true });
      Keyboard.dismiss();
      this.props.forgotPassword(this.state.user_name);
    }
  };

  render() {
    return (
      this.state.resetSuccess ?
        <View style={styles.resetPasswordSuccessContainer}>
          <View>
            <Animated.Image source={require("./../../../assests/images/logo.png")} resizeMode="contain" style={{ width: 200, height: this.imageHeight }} />
          </View>
          <View style={{ padding: 30, alignItems: "center" }}>
            <Text style={styles.resetSuccssText}>Please check registered Email Id or Mobile Number to continue.</Text>
          </View>
          <View style={styles.forgotContainer}>
            <TouchableOpacity style={styles.backToginButton} onPress={() => this.props.navigation.navigate("Login")}>
              <Text style={{ textAlign: "center", height: 40, color: "white", paddingTop: 10 }}>Back to Login</Text>
            </TouchableOpacity>
          </View>
        </View>
        :
        <KeyboardAvoidingView style={styles.loginConatiner} behavior="padding">
          <View style={styles.logoConatiner}>
            <View>
              <Animated.Image source={require("./../../../assests/images/logo.png")} resizeMode="contain" style={{ width: 200, height: this.imageHeight }} />
            </View>
            <View style={{ paddingTop: 30, alignItems: "center" }}>
              <Text style={styles.loginText}>ForgotPassword</Text>
            </View>
          </View>
          <View style={styles.textContainer}>
            <View style={styles.emailConatiner}>
              <View>
                <Image source={require("./../../../assests/images/mail-icon.png")} resizeMode="contain" style={{ width: 18, height: 18, margin: 10 }} />
              </View>
              <View style={styles.emailOuter}>
                <TextInput style={styles.emailInput} onChangeText={text => this.setState({ user_name: text })} value={this.state.user_name} placeholder="Enter Email" keyboardType="email-address" />
                {!this.state.valid ? <Text style={{ color: 'red', marginBottom: 5 }}>*enter valid email id</Text> : null}
                {this.state.resetFailed ? <Text style={{ color: 'red', marginBottom: 5 }}>something went wrong!</Text> : null}
              </View>
            </View>
            <View style={styles.forgotContainer}>
              <TouchableOpacity onPress={() => this.props.navigation.navigate("Login")}>
                <Text style={styles.forgottext}>back to login</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.buttonouter}>
            <TouchableOpacity style={styles.loginButton} onPress={() => this.submit()}>
              <Text style={styles.text}>Submit</Text>
            </TouchableOpacity>
          </View>
          {
            this.props.processingRequest ?
              <View style={{ position: 'absolute', zIndex: 100, height: '100%', width: '100%', justifyContent: "center", alignItems: "center" }} >
                <ActivityIndicator size="large" color="#0000ff" />
              </View>
              : null
          }
        </KeyboardAvoidingView>
    );
  }
}

const mapStateToProps = state => {
  return {
    loggedIn: state.loginReducer.loggedIn,
    forgotPasswordRequestSuccess: state.loginReducer.forgotPasswordRequestSuccess,
    processingRequest: state.loginReducer.processingRequest,
    forgotPasswordRequestFailed: state.loginReducer.forgotPasswordRequestFailed
  };
};

const mapDispatchToProps = {
  forgotPassword
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ForgotPassword);
